import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  PushNotificationIOS,
  Clipboard,
  Alert,
  Switch,
  Image,
  TouchableOpacity,
} from 'react-native';

import VLX, { VeloxityEvent } from 'react-native-veloxitysdk';

export default class Home extends Component<{}> {
  static navigationOptions = ({ navigation, props }) => {
    const { state } = navigation;

    return {
      headerStyle: { backgroundColor: '#841584' },
      headerTitleStyle: { color: 'white' },
      title: 'Veloxity Sample Application',
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      deviceID: '',
      isServiceEnabled: false,
      phoneNumber: '',
    };
  }

  componentWillMount() {
    VLX.on(VeloxityEvent.AuthSucceed, () => {
      console.log('Auth Succeed');
      this.setState({ isServiceEnabled: true });
      this.setDeviceID();
    });

    VLX.on(VeloxityEvent.AuthFailed, () => {
      console.log('Auth Failed');
      this.setState({ isServiceEnabled: false });
      this.setDeviceID();
    });
    if (Platform.OS === 'android') {
      VLX.on(VeloxityEvent.OnCompleted, () => {
        console.log('OnCompleted');
      });
    }

    VLX.getServiceStatus(status => {
      this.setState({ isServiceEnabled: status });
    });
    VLX.sendCustomData({ sampleField: 'Veloxity' });
  }

  componentDidMount() {
    this.setDeviceID();
    if (Platform.OS === 'ios') {
      PushNotificationIOS.requestPermissions();
      PushNotificationIOS.addEventListener('register', function(devicetoken) {
        console.log('token = ', devicetoken);
        VLX.registerDeviceToken(devicetoken);
      });

      PushNotificationIOS.addEventListener('registrationError', () => {
        console.log('An error is occured while registering notification services.');
      });

      PushNotificationIOS.addEventListener('notification', function(notification) {
        console.log('notification = ', notification);
        VLX.startBackgroundTransactionWith(notification._data);
      });
    }

    VLX.setWebServiceUrl('https://sqgz.veloxity.net/');
    VLX.setAuthorizationMenu(
      'Data Usage Permission',
      'For the purposes of benefiting from the services offered by this mobile app I allow Vodafone to process my personal information such as device, subscription, data usage, and location so long I use the app, knowing that I can revoke the permission levels anytime from settings menu of this app.',
      'Accept',
      'Deny',
    );
    if (Platform.OS === 'android') {
      VLX.setPriority('<PRIORITY>');
      VLX.setLicenseKey('<ANDROID_LICENSE_KEY>');
      VLX.setGcmSenderID('<GCM_SENDER_ID>');
    } else if (Platform.OS === 'ios') {
      VLX.setLicenseKey('<IOS_LICENSE_KEY>');
    }
    VLX.start();
  }

  componentWillUnmount() {
    PushNotificationIOS.removeEventListener('register', null);
    PushNotificationIOS.removeEventListener('registrationError', null);
    PushNotificationIOS.removeEventListener('notification', null);
  }

  setDeviceID() {
    VLX.getDeviceId(deviceID => {
      this.setState({ deviceID });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('./logo.png')} />
        <Text style={styles.welcome}>Veloxity React Native!</Text>
        <View style={styles.infoSection}>
          <Text>Your Device ID</Text>
          <TouchableOpacity
            style={{ marginTop: 10 }}
            onPress={() => {
              Clipboard.setString(this.state.deviceID);
              Alert.alert('Information', 'Device id is copied to clipboard');
            }}
          >
            <Text style={styles.deviceID}>{this.state.deviceID}</Text>
          </TouchableOpacity>
          <View style={styles.subSection}>
            <Text>Service Status</Text>
            <Switch
              style={styles.serviceSwitch}
              value={this.state.isServiceEnabled}
              onTintColor="#841584"
              onValueChange={() => {
                if (this.state.isServiceEnabled) {
                  VLX.optOut();
                } else {
                  VLX.optIn();
                }
                this.setState({
                  isServiceEnabled: !this.state.isServiceEnabled,
                });
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  deviceID: {
    color: '#841584',
    fontSize: 16,
  },
  infoSection: {
    alignItems: 'center',
    marginTop: 20,
  },
  subSection: {
    alignItems: 'center',
    marginTop: 20,
  },
  serviceSwitch: {
    marginTop: 10,
  },
  logo: { width: 100, height: 100, position: 'absolute', top: 50 },
});

import React from 'react';
import { View, Text } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Home from './Home';

const RootNavigator = StackNavigator({
  Home: {
    screen: Home,
    title: 'Veloxity Sample Application',
  },
});

export default RootNavigator;
